#Berkay Guenes, Mert Kaya, Adalbrecht Mohsin

`%o%` <- function(f, g) {
  fun <- function(x, ...) {
    f(g(x, ...))
  }
  fun
}
