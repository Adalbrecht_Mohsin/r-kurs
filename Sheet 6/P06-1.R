# Berkay Guenes, Mert Kaya, Adalbrecht Mohsin

library(rlang)

#1
sin <- function(x) {
  (0+1i) * sinh(x * (0-1i))
}
#sin(pi*c(0,0.5,1))

sin_scaled <- function(x) {
  base::sin(pi*x)
}

#sin_scaled(c(0,0.5,1))

#2
for (i in c("sum", "trace", "eigen.values", "det", "inverse")){
  print(exists(i, where="package:base"))
}

#3
y <- 3
f2 <- function() {
  y <- 5
  y + with_env(parent.env(environment()), y)
}
f2()
## 8
y <- 7
f2()
## 12
e2 <- env(y = 0)
with_env(e2, f2())
## 12

#4
env_bind_new <- function(e = NULL, vars) {
  if(is.null(e)){
    e <- parent.env(environment())
  }
  for(i in names(vars)){
    if (!is.null(e$i)){
      print(paste0("Warning:", i, "already exists in environment at ", e, ". Cannot replace it"))
      vars <- vars[!names(vars) %in% c(i)]
    }
  }
  env_bind(e, !!!v)
}
e1 <- env(x = 1, y = 2, z = 3)
v <- c(x = 0, y = 99, w = 4)
env_bind_new(e1, v)
e1$x
e1$w

#5
library(stringr)
f <- function() {
  p <- sample(1:100, size = 1)
  q <- sample(1:100, size = 1)
  ef <<- environment()
  return(p*q)
}
# here
pq <- f()
pq
str_glue("The product of {ef$p} and {ef$q} is {ef$p * ef$q}.")