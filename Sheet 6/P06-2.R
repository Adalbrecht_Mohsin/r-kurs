#Aufgabe 6.2: Berkay Guenes, Mert Kaya, Adalbrecht Mohsin
library(rlang)
#1x
e1 <- new.env(parent=emptyenv())
e2 <- new.env(parent=e1)
identical(emptyenv(), parent.env(e1)) #Ist 'emptyenv' parent von 'e1'?
identical(e1, parent.env(e2)) #Ist 'e1' parent von 'e2'?

e1$a <- 2
e1$f <- function(x)
ls.str(e1)  
  
e2$b <- 5
e2$e <- e1$a
ls.str(e2)

#2x
rm(e2)
rm(e1)
  #Schritt 1 beginnt
  y <- 5
  e1 <- new.env(parent=globalenv())
  e1$a <- 1 
  e1$y <- y
  #Schritt 2 beginnt
  f <- function (x) {print(x+a)}
  f(y)#*      
  #Schritt 3 beginnt
  e1$y <- 3
  environment(f) <- e1
  f(y)#**  
  
  # Für f(y) in (2) erwarten wir, dass das Objekt 'a' nicht gefunden wird, während
  # wir für f(y) in (3) die Ausgabe '6' erwarten. Wählen wir f(e1$y) in (3) erwarten wir
  # die Ausgabe '4'