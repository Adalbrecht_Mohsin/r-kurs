# Berkay Guenes, Mert Kaya, Adalbrecht Mohsin

library(tidyverse)

states <- read_csv("us_states.csv")
revenues <- read_delim("revenues.csv", delim = ";") # Aus gutem Grund wurde hier
# kein Komma als Spaltenbegrenzung benutzt.
cities <- read_csv("us_cities.csv")
employers <- read_csv("employers.csv")
shootings <- read_csv("mass_shootings_2022.csv")
marketCap <- read_csv("companies_mc.csv")

states %>% mutate_if(is.character, str_trim) -> states
revenues %>% mutate_if(is.character, str_trim) -> revenues
cities %>% mutate_if(is.character, str_trim) -> cities
employers %>% mutate_if(is.character, str_trim) -> employers
shootings %>% mutate_if(is.character, str_trim) -> shootings
marketCap %>% mutate_if(is.character, str_trim) -> marketCap

# 1.
# für die Billionen
marketCap$market_cap %>%
    str_detect("T") -> trillion

marketCap$market_cap[trillion] %>%
    str_extract("[0-9]*\\.[0-9]*") %>%
    as.double() * 1e3 -> marketCap$market_cap[trillion]

# für die Milliarden
marketCap$market_cap %>%
    str_detect("B") -> billion

marketCap$market_cap[billion] %>%
    str_extract("[0-9]*\\.[0-9]*") %>%
    as.double() -> marketCap$market_cap[billion]

# 2.
revenues$rev_change %>%
    str_replace_all(c("Increase " = "+", "Decrease " = "-", "%" = "")) %>%
    as.double() / 100 + 1 -> revenues$rev_change

# 3.
rev_ranks <- tibble(company = revenues$company,
    revenue_2020 = (revenues$revenue_2021 / revenues$rev_change),
    revenue_2021 = revenues$revenue_2021)

rev_ranks$rank_2020 <- order(rev_ranks$revenue_2020, decreasing = TRUE)
rev_ranks$rank_2021 <- order(rev_ranks$revenue_2021, decreasing = TRUE)

rev_ranks$rank_change <- rev_ranks$rank_2020 - rev_ranks$rank_2021
rev_ranks <- rev_ranks[order(rev_ranks$rank_change, decreasing = TRUE), ]

print(sprintf("%s ist mit %i Plätzen von 2020 auf 2021 die Rangfolge am weitesten nach oben geklettert.",
    rev_ranks$company[1],
    rev_ranks$rank_change[1]))

print(sprintf("%s ist mit %i Plätzen von 2020 auf 2021 die Rangfolge am weitesten nach unten gefallen.",
    tail(rev_ranks$company, n = 1)[1],
    abs(tail(rev_ranks$rank_change, n = 1)[1])))

# 4.
revenues$headquarter %>%
    str_split_fixed(", ", n = 2) -> headquarters

revenues$hq_city <- headquarters[, 1]
revenues$hq_state <- headquarters[, 2]
revenues <- revenues[, !names(revenues) %in% "headquarter"]

# 5.
semi_join(revenues, states, by = c("hq_city" = "capital")) %>%
    nrow() -> comps_in_cap

print(sprintf("Es haben %i Unternehmen ihren Firmensitz in der Hauptstadt eines States.",
    comps_in_cap))

# 6.
left_join(employers, revenues, by = c("employer" = "company")) %>%
    dplyr::select(employer, sector, employees, revenue_2021) -> employee_rev

employee_rev$rev_p_employee <- employee_rev$revenue_2021 / employee_rev$employees

na_mask <- is.na(employee_rev$rev_p_employee)

employee_rev[!na_mask, ] %>%
    group_by(sector) %>%
    summarize(mean_per_cap_rev = mean(rev_p_employee)) -> emp_rev_sector

emp_rev_sector <- emp_rev_sector[order(emp_rev_sector$mean_per_cap_rev, decreasing = TRUE), ]

employee_rev[na_mask, ]$rev_p_employee <- min(employee_rev[!na_mask, ]$rev_p_employee) / 2
employee_rev <- employee_rev[order(employee_rev$rev_p_employee, decreasing = TRUE), ]

print(sprintf("Das Unternehmen mit dem höhsten Umsatz pro Mitarbeitenden ist %s.",
    employee_rev[1, 1]))

#7.

big_caps <- inner_join(states, cities, by = c("capital" = "city"))

group_by(cities, state) %>%
    mutate(state_rank = row_number()) %>%
    semi_join(big_caps, by = c("city" = "capital")) %>%
    dplyr::select(city, state, state_rank) -> big_caps

# 8.
group_by(shootings, State) %>%
    summarise(dead = sum(`# Killed`)) %>%
    right_join(states, by = c("State" = "name")) %>%
    dplyr::select(State, dead, population) -> shooting_state_dead

shooting_state_dead$dead_p_cap <- shooting_state_dead$dead / shooting_state_dead$population

shooting_state_dead <- shooting_state_dead[order(shooting_state_dead$dead_p_cap), ]
