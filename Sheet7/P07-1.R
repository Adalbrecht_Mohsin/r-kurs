#Berkay Guenes, Mert Kaya, Adalbrecht Mohsin

plus <- function(x) {function(y) x+y}
pluses <- vector("list",5)

for (i in seq_along(pluses)) pluses[[i]] <- plus(i)
pluses[[1]](1)

# Lazy evaluation führt dazu, dass i mit dem letzten zugeteilten Wert, i.e. 5
# evaluiert wird. Bei z.B. Zuteilung von i <- 10 wären nun pluses[[1]] immer
# noch die Funktion, die 5 addiert, aber pluses[[2]] die Funktion, die 10
# addiert usw.

# plus ist reinlich 

doubled <- numeric(5)
for (i in seq_along(pluses)) doubled[i] <- pluses[[i]](i)
doubled

# pluses[[1]] wurde schon als die Funktion, die 5 addiert, oben evaluiert;
# das erklärt, weswegen doubled[1]=6, für i!=1 wird aber jeweils
# pluses[[i]](i) direkt ausgewertet, s.d. doubled[i] erwarteten Wert hat.

true_plus <- function(x) {force(x); function(y) x+y}
