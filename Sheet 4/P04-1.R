# Berkay Guenes, Mert Kaya, Adalbrecht Mohsin

library(stringr)

s <- "Hurra: R-Programmierung bereitet Freude."

# 1.
unique_len <- length(unique(str_extract_all(s, "[:alpha:]")[[1]]))
print(sprintf("Your string has %i diffrent letters.", unique_len))

# 2.
str_len <- length(str_extract_all(s, "[:alpha:]")[[1]])
print(sprintf("Your string has %i letters in total.", str_len))

# 3.
tot_len <- str_length(s)
not_letters <- tot_len - str_len
print(sprintf("Your string has %i characters that are not letters.",
                not_letters))
